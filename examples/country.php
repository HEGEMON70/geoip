#!/usr/bin/php -q
<?php

// This code demonstrates how to lookup the country by IP Address

include("../src/geoip.inc");

// Uncomment if querying against GeoIP/Lite City.
// include("../src/geoipcity.inc");

$gi = geoip_open("../bbdd/GeoIP.dat", GEOIP_STANDARD);

echo geoip_country_code_by_addr($gi, "79.144.30.45") . "\t" .
    geoip_country_name_by_addr($gi, "79.144.30.45") . "\n";

geoip_close($gi);
